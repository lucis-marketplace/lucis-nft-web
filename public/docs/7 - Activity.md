# Activity

## What is Activity

An activity is an event that happend on the market. Your almost every operation will generate an activity, including Listing, Bidding, and Sale. You can query your own activities in your Profile page, to track NFTs you once sold or bidded.

Another way to use activity is to add the NFT you are interested at into Favorites. You do this by click the **Like** button of each NFT. All activities related to the NFT you like can be tracked in your Profile page too.

For activities of some types, LucisNFT sends notification to you via Telegram based on your settings. 

## List of Activity Types

-   Listing as auction
-   Listing as fixed price
-   Listing price changed
-   Listing cancelled
-   Listing expired
-   Sale as fixed price
-   Sale as auction
-   Sale, buy accept
-   Offer bidded
-   Offer, buy offer
-   Offer expired
-   Offer cancelleds
-   Send 