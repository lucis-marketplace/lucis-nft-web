# Auction

## Auction Sale

Auction begins from the starting price and everyone can place bids over a set period of time. The highest/last bid during that period will win your NFT.


## Rules

### Cancellation

-   Auction is only cancellable when it has no bids.
-   Bids are not cancellable.

### End Time

-   When the ending countdown goes to 0, the auction ends. The item can be collected by the winner (the last bidder remaining) or the seller.
-   When a new bid comes in and the countdown is less than 10 minutes, the time will be extended by 10 minutes.

### Bid

-   Each bid must be at least 5% higher than its previous bid.


### Collect

-   After an auction ends with valid bids, either the seller or the bidder needs to click **Collect** button on NFT’s detail page to finish the transfer of NFT and payment to each other.

### Fee

-   5% of the last bids will be taken as market fee.

___

## Start an Auction


A complete flow to sell in Auction is like below:

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc11.png)

### Step 1 - Set Start Price & End Date

A start price is the price from which bidders start bidding, and the first bid is required to be 5% more than the start price. If you are not sure how much the start price should be, you can search for similar NFTs auctioning on LucisNFT and decide based on others’ prices.

An end date specifies when the auction ends. You can select from 1 day, 3 days, and 5 days later.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc12.png)

### Step 2 - Approval for NFT

After the **Start Listing** button is clicked, you will be guide to finish an approval operation.

Same like Fixed Price Listing, **Approval** is like giving the LucisNFT the permission to transfer your NFT to someone else. Don’t worry, no real transfer would happen at this moment. When someone placed a bid for your NFT, the contract of LucisNFT would then transfer your NFT to the auction contract immediately to make sure the item is safe before the auction ends.

Approval is a one-time only operation for each NFT collection.

### Step 3 - Start Auction

After the approval, you will be prompt by your wallet to finish a signing. After then your auction would be listed on the market in a few seconds.

The signing takes no fee, and is used to verify that you are the owner of the listing NFT.

All your auctions are listed in the **On Sale** section of your Profile.

**The only case that you can cancel your on-going auction is that when no bids is placed yet.**

### Step 4 - Auction Ends with Bids

There are two ways to end an auction:

-   The auction’s time goes out;
-   Manually end the auction before time goes out.

Once an auction ends, the last bid wins.The auction item (your NFT) and the bid payment will be transferred to each other automatically.


Ended auctions are listed in the **Collected** section of your Profile, under the Ended tab.

It’s recommended to enable the Telegram Notification in Settings page to receive notifications when your auction receives bid or ends.

___

## Bid in an Auction

### Browse

You can use Filter feature on market and collection page to filter out all auctions.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc13.png)

Each grid in the list is an auctioning NFT. On the right side of NFT’s name is the current auction price. The hammer mark means it’s an auction.

Below the auction price, there is an ending time which means the remaining time of the auction.

### Place an auction

To join an auction, you can place an auction on any on-going auction. On the detail page of an auction, you can see the current auction price, and a  **AUC** button.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc14.png)

You can also see the history auctions in the auctions tab below the description of the NFT. auction with the **Lead** mark is the currently highest auction.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc15.png)

Click the **AUC** button and you will see a popup, where you can fill in an auction price that you would like to offer. And you can check the current minimum auction price below the price input.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc16.png)

Click the "Approve" button in the popup, then your wallet should prompt to ask you to confirm the transfer/auction.

Once the payment is confirmed, a confirmation dialog should shows on the page indicating the auctionding is finished. You can visit your Profile page to see the auction you just placed.

### Finish an Auction

When you win an auction, the item will go to you automatically.

After the transfer is confirmed, you can browse the NFT in your Profile page.