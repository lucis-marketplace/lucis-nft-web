# Collection

## What is Collection

A collection is a group of NFTs minted under the same contract on chain, like CryptoPunks or Polychain Monsters.

Usually NFTs from same collection share the same attribute types. For example, Polychain Monsters NFTs have a **Special** attribute. An NFT with Special == YES attributes are very rare and are usually listed on market at very expensive prices.

_Advanced filter is currently limited to Verified Collections only_

## Verified Collection

Verified collections are:

-   Reliable, NFTs in the collection are unlikely to be fake;
-   Filter-friendly, you can search and filter them not only in market, but also in your own wallet;
-   Royalty supported, if there is a royalty settings for the collection, each trade on LucisNFT follows its settings and distribute royalty to the team.

Verified collections are highlighted on the homepage of LucisNFT. And you could browse the full list in the NFT Ranking page.

If you are a member of an NFT collection, get your collection verified via [this link