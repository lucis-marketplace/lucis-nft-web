# Notification

Based on your and your liked NFTs’ activities, LucisNFT sends notification to you via Telegram bot, eg. Item sold, Outbidded, etc.

It’s recommend for everyone to enable this feature so not to miss anything important.

## How to Enable Notification


1.  Make sure your Telegram app is openned and running;
2.  Click the avatar at the right top of LucisNFT, then select to open Settings page;
3.  Select Notification from left tabs;
4.  Toggle the switch at the right side of the Telegram title;
5.  Click the **Connect** button, which should bring Telegram to the front and open a chat with LucisNFT’s Telegram Bot;
6.  In the chat, click the **Start** button at the buttom
7.  Back to LucisNFT’s page, copy the code(eg. Lucis-9999) on the popup
8.  Paste the code in the Telegram chat and hit enter.
9.  The binding should be done. Notifications will start sending.

## Notification Types

Notifications related to you

-   You listed an item;
-   Your auction ends with/without bids;
-   Your item sold;
-   You won an auction;
-   You are outbidded by others;
-   You received a buy offer;
-   You accepted a buy offer;
-   Your buy offer is accpeted;
-   Your buy offer is cancelled;
-   Your item is transferred to others.

Notification releated to your favorited NFT

-   NFT is listed;
-   NFT’s price is changed;
-   NFT sale is cancelled;
-   NFT auction ends with/without bids;
-   NFT is sold;
-   NFT auction received a bid;
-   NFT received a buy offer;
-   NFT is transferred.