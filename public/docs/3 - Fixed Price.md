# Fixed Price

Fixed Price sale is a standard sale, where you set a price at which your NFT is listed and others can pay to purchase it. The payment deducting market fees and royalties is the total revenue you get.

## Rules

### Cancellation

Fixed Price Listing can be cancelled anytime before purchased.

### Expiration

-   If an expiration time is set to the listing, the listing will be removed once the time is up.
-   If the NFT for sale is move away from its owner’s address, the listing will be removed once detected, or invalidated when someone trys to purchase it.

### Fee

-   LucisNFT takes a fixed 5% market fee.
-   Depending on the royalty settings, there may be an extra royalty fee taken from the payment by the NFT team.

## Sell as Fixed Price


A complete flow to sell one NFT in Fixed Price is like below:

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc2.png)

### Step 1 - Set Price 

Setting a Price for your NFT could be very straightforward. It’s recommended to set a price with the default currency, though LucisNFT supports multi-currency which is a M2 feature.

If you are not sure how much your NFT worth, you can search for similar NFTs selling on LucisNFT and decide based on others’ prices.


![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc3.png)

### Step 2 - Approval

After the **Start Listing** button is clicked, you will be guide to finish an approval operation.

For newbies to blockchain or NFTs, **Approval** is like giving the LucisNFT the permission to transfer your NFT to someone else. Don’t worry, no real transfer would happen at this moment. When someone payed money for your NFT, the contract of LucisNFT would then transfer your NFT to the buyer immediately so the trade could be finished.

Approval is a one-time only operation for each NFT collection.

### Step 3 - Start Listing

After the approval, you will be prompt by your wallet to finish a signing. After then your item would be listed on the market in a few seconds.

The signing takes no fee, and is used to verify that you are the owner of the listing NFT.

You can always find all your listings in the **On Sale/selling** section of your Profile.

_It’s possible to cancel your fixed price listing anytime before sold_

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc4.png)

### Step 4 - Receive Payment

When someone purchased your NFT, the NFT is automatically transferred to the buyer, while the payment transferred to you in the mean time.

History of sales can be found in the **Activities** section of your Profile.

It’s recommended to enable the Telegram Notification in Settings page to receive notifications when your items get sold.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc5.png)

___

## How to Buy

### Browse

You can use Filter feature on market and collection page to filter out all Fixed Price listings.

Each grid in the list is a fixed price listing NFT. On the right side of NFT’s name is the sale price. The $ mark means it’s a fixed price sale.

Below the sale price, there could be a number indicating either one of these:

-   Floor price, the lowest traded price for other NFTs of the same contract, within 7 days;
-   Last price, price of the last sale of this NFT;

### Purchase

On the detail page of a fixed price listed NFT, you can see the fixed price for sale, and a  **Buy** button.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc8.png)

To purchase an NFT, click the **Buy** button and you will see a confirm popup. Click the Buy button in the popup, then your wallet should prompt to ask you to confirm the transfer/payment.

Once the payment is confirmed, a confirmation dialog should shows on the page indicating the purchase is finished and NFT is transferred to you already. You can visit your Profile page to see the NFT you just obtained.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc9.png)