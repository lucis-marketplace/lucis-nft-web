# How to Sell NFTs on LucisNFT

## Preparation

Before start selling your NFTs, please make sure:

-   NFTs you want to sell is ERC-721 compatible. ERC-1155 NFTs are not supported yet.
-   There is at least enough native token (e.g. BNB on BSC) in wallet to pay for gas fee of approval.
-   LucisNFT supports Fixed Price Listing and Auction to sell NFTs, you can choose either way, but you can’t sell an NFT in both ways at the same time.

To sell your NFTs, you need to visit Profile page where lists all NFTs in your wallet. The Profile page features powerful filters to help you find the NFT to sell.

After open the detail page of the NFT, you can find the Fixed price button as well as Auction button. You can also click the **…** button to reveal more commands like Send token, etc.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc1.png)

___

## Choose a Way to Sell

-   Fixed Price
-   Auction
-   Accept Buy Offers