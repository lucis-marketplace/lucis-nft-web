# Welcome to LucisNFT
![image](/meta-banner.jpg)

LucisNFT is a fully featured decentralized marketplace for buying, selling and trading NFTs and deployed on multiple blockchains.


## Getting Started

To start trading on LucisNFT, you need to have use a wallet that is supported. At the moment, LucisNFT supports the wallet below:

To sell NFT, you should visit the Profile page to browse all NFTs you have. LucisNFT implemented a very powerful Profile that helps you search and filter among NFTs in your wallet, and track all activities related to your NFT and wallet.

There are mainly 2 ways to trade an NFT on LucisNFT:

-   Fixed Price Listing
-   English Auction(with incentive bidding)


After listing your NFT, it’s recommended to enable the Notification via Telegram feature, so any purchase or bid in auctions will trigger an notification sent to you by our Telegram bot.

## For NFT Builders

LucisNFT is designed to meet the most needs of the NFT Builders. And builder teams can contact us to get verified.

Getting your NFT verified can provide you:

-   Highlights on homepage
-   filtering & searching
-   Royalty sharing (For teams with volume over 50 BNB or equivalent vol on other chains)
-   Payment in your token (For teams meeting our requirements)

If you are a member from NFT teams, please read our detailed introduction about Verified NFT Collection here. To get your NFT verified, fill the form and we will contact you soon.