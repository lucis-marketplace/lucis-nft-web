# Buy Offer

Buy Offer is an offer sent from buyers who are willing to buy an NFT with a price. It’s not mandatory, receivers or sellers can accept it, reject it, or just ignore it. Therefore it’s recommended to investigate the market average price before making your offer. The higher price you set, the higher chance that seller may accept your offer.


![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc17.png)

## Buy Offer Rules

### Eligibility

Buy offers can be sent to 2 kinds of NFTs:

-   Fixed Price Listing NFTs
-   Unlisted NFTs

Auctioning NFTs can’t accept buy offers. If the NFT to which you sent a buy offer before goes into an auction sale, the seller won’t be ablt to accept or reject your offer, so you need to cancel your own.

### Cancellation

Buy offer can be cancelled anytime before accepted.

### Expiration

-   Buy offer can be set with an expiration time. The offer will be cancelled once the time is up.
-   If the NFT for sale is move away from its owner’s address, or sold to someone else, the buy offer will remain valid. Its new owner can still accept the buy offer.

### Fee

-   LucisNFT takes a fixed 5% market fee.

## Make a Buy Offer

### Browse

The first step before making a buy offer is to find the NFT you really want. Except for those listed NFT on LucisNFT, you can also access every sellers’ Profile page to see the unlisted NFTs.

### Make Buy Offer

In the detail page, there should be a large blue **Make Offer** button for unlisted NFTs, or a clickable text link below sale price for any Fixed Price Listing. Click and you will see a popup where you can fill in the offer price and expiration date. Make sure you do have the amount of money a little more than that in your wallet or the next step will fail.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc18.png)

Click the Offer to Buy button in the popup, then your wallet should prompt to ask you to confirm the transfer/payment.

Once the payment is confirmed, a confirmation dialog should shows on the page indicating the offer is made. You can click the Transaction ID to view the actual transaction on chain, or visit your Profile page to see the offer you just made.

![image](https://lucis-image.s3.ap-southeast-1.amazonaws.com/doc19.png)

## Accept a Buy Offer

For both your listed and unlisted NFTs, other ones may send you buy offers. A Buy Offer is like a query asking would you want to sell the NFT at that price. If you accept it, then sale will be done immediately, your NFT send to others and you receive the payment. You could also reject an buy offer if you are not very interested in it.