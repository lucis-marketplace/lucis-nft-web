export const API_URL = process.env.NEXT_PUBLIC_API_URL
export const TEST_API_URL = process.env.NEXT_PUBLIC_TEST_API_URL
export const LUCIS_CONTRACT_ADDRESS =
  process.env.NEXT_PUBLIC_LUCIS_MARKETPLACE_CONTRACT_ADDRESS
export const BSC_SCAN_ADDRESS = `https://bscscan.com/address/`
export const BSC_SCAN_TRANSACTION = `https://bscscan.com/tx/`
