export type TPaginateParams = {
  limit?: number
  offset?: number
  reverse?: boolean
  order_by?: string
}

export type TNetworkParams = {
  blockchain_id?: number
}
