export type TCollection = {
  id?: number
  name?: string
  photo?: string
  cover_photo?: string
  description?: string
  stats?: any
  is_verified?: number
}
