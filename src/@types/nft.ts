export type TNftItem = {
  id?: number
  name?: string
  price?: number
  photo?: string
  owner?: string
  contract_name?: string
  collection_id?: number
  blockchain_id?: number
  network?: string
  is_verified?: boolean
  inventory_status?: number
  endTime?: string
  hidePrice?: boolean
  topAuc?: number
  symbol?: string
}
