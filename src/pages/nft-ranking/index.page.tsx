import { ListingBar } from "../../components/Home/ListingBar"
import { RankingList } from "../../components/Nft-Ranking/RankingList"

const NftRanking = () => {
  return (
    <div className="nft-ranking">
      <ListingBar />
      <RankingList />
    </div>
  )
}
export default NftRanking
