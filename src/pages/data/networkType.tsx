export const networkType = [
  {
    label: (
      <div className="network">
        <img src="/common/all-network.png" alt="" />
        All network
      </div>
    ),
    value: 0,
  },
  {
    label: (
      <div className="network">
        <img src="/networks/bsc-testnet.svg" alt="" />
        BSC Testnet
      </div>
    ),
    value: 1,
  },
  {
    label: (
      <div className="network">
        <img src="/networks/bsc.svg" alt="" />
        BSC Mainnet
      </div>
    ),
    value: 2,
  },
  // {
  //   label: (
  //     <div className="network">
  //       <img src="/common/bnb-logo.png" alt="" />
  //       BNB Chain
  //     </div>
  //   ),
  //   value: "BNB Chain",
  // },
  // {
  //   label: (
  //     <div className="network">
  //       <img src="/common/ethereum.png" alt="" />
  //       Ethereum
  //     </div>
  //   ),
  //   value: "Ethereum",
  // },
  // {
  //   label: (
  //     <div className="network">
  //       <img src="/common/celo.png" alt="" />
  //       Celo
  //     </div>
  //   ),
  //   value: "Celo",
  // },
  // {
  //   label: (
  //     <div className="network">
  //       <img src="/common/aurora.png" alt="" />
  //       Aurora
  //     </div>
  //   ),
  //   value: "Aurora",
  // },
  // {
  //   label: (
  //     <div className="network">
  //       <img src="/common/arbitrum.png" alt="" />
  //       Arbitrum
  //     </div>
  //   ),
  //   value: "Arbitrum",
  // },
  // {
  //   label: (
  //     <div className="network">
  //       <img src="/common/fantom.png" alt="" />
  //       Fantom
  //     </div>
  //   ),
  //   value: "Fantom",
  // },
]
